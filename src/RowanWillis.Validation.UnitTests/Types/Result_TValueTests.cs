﻿using Xunit;

namespace RowanWillis.Validation.UnitTests;

public class Result_TValueTests
{
    private readonly Problem Problem = new("Problem.Type1", "Problem summary 1");
    private readonly Problem DistinctProblem = new("Problem.Type2", "Problem summary 2");
    private readonly object Value = new();

    [Fact]
    public void GivenAValue_ASuccessfulResult_ItIsSuccessful()
    {
        var result = Result.Success(Value);

        Assert.True(result.IsSuccess);
    }

    [Fact]
    public void GivenAValue_ASuccessfulResult_ContainsTheGivenValue()
    {
        var result = Result.Success(Value);

        Assert.Equal(Value, result.Value);
    }

    [Fact]
    public void GivenAValue_ASuccessfulResult_WhenAccessingTheProblems_ThenAnExceptionIsThrown()
    {
        var result = Result.Success(Value);

        Assert.Throws<InvalidResultProblemAccessException>(() => result.Problems);
    }

    [Fact]
    public void GivenAFailureResult_WhenAccessingTheValue_ThenAnExceptionIsThrown()
    {
        var result = Result.Failure<object>(Problem);

        var exception = Assert.Throws<InvalidResultValueAccessException>(() => result.Value);
        Assert.NotEmpty(exception.Message);
    }

    [Fact]
    public void GivenASingleProblem_AFailureResult_IsNotSuccessful()
    {
        var result = Result.Failure<object>(Problem);

        Assert.False(result.IsSuccess);
    }

    [Fact]
    public void GivenASingleProblem_AFailureResult_ContainsTheGivenProblem()
    {
        var result = Result.Failure<object>(Problem);

        Assert.NotNull(result.Problems);
        Assert.Contains(Problem, result.Problems);
    }

    [Fact]
    public void GivenMultipleProblems_AFailureResult_ItIsNotSuccessful()
    {
        var problems = new[] { Problem, DistinctProblem };

        var result = Result.Failure<object>(problems);

        Assert.False(result.IsSuccess);
    }

    [Fact]
    public void GivenMultipleDistinctProblems_AFailureResult_ContainsAllDistinctProblems()
    {
        var problems = new[] { Problem, DistinctProblem };

        var result = Result.Failure<object>(problems);

        Assert.Equal(problems.Length, result.Problems.Count());

        foreach (var problem in problems)
            Assert.Contains(problem, result.Problems);
    }

    [Fact]
    public void GivenMultipleEqualProblems_AFailureResult_ContainsOnlyASingleInstanceOfTheProblem()
    {
        var expectedProblem = Problem;

        var result = Result.Failure<object>(new[] { expectedProblem, expectedProblem });

        var problem = Assert.Single(result.Problems);
        Assert.Equal(expectedProblem, problem);
    }
}