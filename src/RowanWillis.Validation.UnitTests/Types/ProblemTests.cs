﻿using Xunit;

namespace RowanWillis.Validation.UnitTests;

public class ProblemTests
{
    public static readonly TheoryData<string, string> ProblemData = new()
    {
        { "Entity.NotFound", "The specified entity does not exist." }
    };

    [Theory]
    [MemberData(nameof(ProblemData))]
    public void ANewProblem_HasTheGivenType(string type, string summary)
    {
        var problem = new Problem(type, summary);

        Assert.Equal(type, problem.Type);
    }

    [Theory]
    [MemberData(nameof(ProblemData))]
    public void ANewProblem_HasTheGivenDescription(string type, string summary)
    {
        var problem = new Problem(type, summary);

        Assert.Equal(summary, problem.Description);
    }

    #region Equality Overrides

    [Theory]
    [MemberData(nameof(ProblemData))]
    public void AProblem_IsNotEqualToNull(string type, string summary)
    {
        var problem = new Problem(type, summary);

        Assert.NotNull(problem);

        // These are included because the == operator is overridden
        Assert.False(problem == null);
        Assert.False(null == problem);
    }

    [Theory]
    [MemberData(nameof(ProblemData))]
    public void AProblem_IsNotEqualToAnObjectOfADifferentType(string type, string summary)
    {
        var problem = new Problem(type, summary);

        Assert.NotEqual(new object(), problem);
    }

    [Fact]
    public void TwoProblems_WithTheSameType_AreEqual()
    {
        var problem1 = new Problem("Problem.Type.A", "Problem description 1");
        var problem2 = new Problem("Problem.Type.A", "Problem description 2");

        Assert.Equal(problem1, problem2);

        // This is included because the == operator is overridden
        Assert.True(problem1 == problem2);
    }

    [Fact]
    public void TwoProblems_WithTheSameType_HaveTheSameHashCode()
    {
        var problem1 = new Problem("Problem.Type.A", "Problem description 1");
        var problem2 = new Problem("Problem.Type.A", "Problem description 2");

        Assert.Equal(problem1.GetHashCode(), problem2.GetHashCode());
    }

    [Fact]
    public void TwoProblems_WithTDifferentTypes_AreNotEqual()
    {
        var problem1 = new Problem("Problem.Type.A", "Problem description");
        var problem2 = new Problem("Problem.Type.B", "Problem description");

        Assert.NotEqual(problem1, problem2);

        // This is included because the != operator is overridden
        Assert.True(problem1 != problem2);
    }

    #endregion
}