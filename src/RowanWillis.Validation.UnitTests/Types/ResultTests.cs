﻿using Xunit;

namespace RowanWillis.Validation.UnitTests;

public class ResultTests
{
    private readonly Problem Problem = new("Problem.Type1", "Problem summary 1");
    private readonly Problem DistinctProblem = new("Problem.Type2", "Problem summary 2");

    [Fact]
    public void ASuccessResult_IsSuccessful()
    {
        var result = Result.Success();

        Assert.True(result.IsSuccess);
    }

    [Fact]
    public void GivenASuccessResult_WhenAccessingTheProblems_ThenAnExceptionIsThrown()
    {
        var result = Result.Success();

        var exception = Assert.Throws<InvalidResultProblemAccessException>(() => result.Problems);
        Assert.NotEmpty(exception.Message);
    }

    [Fact]
    public void GivenASingleProblem_AFailureResult_IsNotSuccessful()
    {
        var result = Result.Failure(Problem);

        Assert.False(result.IsSuccess);
    }

    [Fact]
    public void GivenASingleProblem_AFailureResult_ContainsTheGivenProblem()
    {
        var result = Result.Failure(Problem);

        Assert.NotNull(result.Problems);
        Assert.Contains(Problem, result.Problems);
    }

    [Fact]
    public void GivenMultipleProblems_AFailureResult_ItIsNotSuccessful()
    {
        var problems = new[] { Problem, DistinctProblem };

        var result = Result.Failure(problems);

        Assert.False(result.IsSuccess);
    }

    [Fact]
    public void GivenMultipleDistinctProblems_AFailureResult_ContainsAllDistinctProblems()
    {
        var problems = new[] { Problem, DistinctProblem };

        var result = Result.Failure(problems);

        Assert.Equal(problems.Length, result.Problems.Count());

        foreach(var problem in problems)
            Assert.Contains(problem, result.Problems);
    }

    [Fact]
    public void GivenMultipleEqualProblems_AFailureResult_ContainsOnlyASingleInstanceOfTheProblem()
    {
        var expectedProblem = Problem;

        var result = Result.Failure(new[] { expectedProblem, expectedProblem });

        var problem = Assert.Single(result.Problems);
        Assert.Equal(expectedProblem, problem);
    }
}