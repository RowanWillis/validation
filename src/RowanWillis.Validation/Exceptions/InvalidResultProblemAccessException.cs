﻿namespace RowanWillis.Validation;

public class InvalidResultProblemAccessException : Exception
{
    public override string Message => "Result is not in a state that contains any problems.";
}