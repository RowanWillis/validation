﻿namespace RowanWillis.Validation;

public class InvalidResultValueAccessException : Exception
{
    public override string Message => "Result is not in a state that contains a value.";
}
