﻿namespace RowanWillis.Validation;

public class Result
{
    public static Result Success() => new(true, Array.Empty<Problem>());

    public static Result Failure(Problem problem)
        => new(false, new[] { problem });

    public static Result Failure(IEnumerable<Problem> problems)
        => new(false, problems);

    public static Result<TValue> Success<TValue>(TValue value)
        => new(true, value, Array.Empty<Problem>());

    public static Result<TValue> Failure<TValue>(IEnumerable<Problem> problems)
        => new(false, default!, problems);

    public static Result<TValue> Failure<TValue>(Problem problem)
        => new(false, default!, new[] { problem });

    protected Result(bool isSuccess, IEnumerable<Problem> problems)
    {
        IsSuccess = isSuccess;
        _Problems = new(problems);
    }

    public bool IsSuccess { get; }

    private readonly HashSet<Problem> _Problems = new();

    public IEnumerable<Problem> Problems
        => !IsSuccess ? _Problems : throw new InvalidResultProblemAccessException();
}