﻿namespace RowanWillis.Validation;

public class Problem
{
    public Problem(string type, string description)
    {
        Type = type;
        Description = description;
    }

    public string Type { get; }

    public string Description { get; }

    protected virtual IEnumerable<object?> EqualityComponents => new[] {
        Type
    };

    public override bool Equals(object? obj)
        => obj is not null
            && GetType() == obj.GetType()
            && EqualityComponents.SequenceEqual(((Problem)obj).EqualityComponents);

    public override int GetHashCode()
        => EqualityComponents
            .Aggregate(1, (current, obj) => HashCode.Combine(current, obj));

    public static bool operator ==(Problem? a, Problem? b)
    {
        if (a is null && b is null)
            return true;

        if (a is null || b is null)
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Problem? a, Problem? b)
        => !(a == b);
}