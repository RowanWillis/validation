﻿namespace RowanWillis.Validation;

public class Result<TValue> : Result
{
    internal Result(bool isSuccess, TValue value, IEnumerable<Problem> problems)
        : base(isSuccess, problems)
    {
        _Value = value;
    }

    private readonly TValue _Value;

    public TValue Value => IsSuccess ? _Value : throw new InvalidResultValueAccessException();
}